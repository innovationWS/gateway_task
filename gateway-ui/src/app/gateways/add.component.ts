﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ValidatorFn,AbstractControl,FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';

import { GatewayService, AlertService } from '@app/_services';


@Component({ templateUrl: 'add.component.html' })
export class AddComponent implements OnInit {
    form: FormGroup;
    id: string;
  
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private gatewayService: GatewayService,
        private alertService: AlertService
    ) {}

    ngOnInit() {
        this.id = this.route.snapshot.params['id'];
        this.form = this.formBuilder.group({
            gatewayName: ['', Validators.required],
            ipAddress: ["", [Validators.required, this.customValidator()]]

        });
    }

    get ipAddress(): FormControl {
        return this.form.get("ipAddress") as FormControl;
      }

   customValidator(): ValidatorFn {
        return (control: AbstractControl) => {
          const regex = /^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/;
      
          if (regex.test(control.value)) {
            return null;
          }
      
          return { ipError: true };
        };
      }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;
        this.addGateway();
        
    }

    private addGateway() {
        this.gatewayService.create(this.form.value)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('GateWay added', { keepAfterRouteChange: true });
                    this.router.navigate(['../'], { relativeTo: this.route });
                },
                error: error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            });
    }  
}