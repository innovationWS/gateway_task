﻿import { Component, OnInit } from '@angular/core';
import { GatewayService } from '@app/_services';


@Component({ templateUrl: 'list.component.html' })
export class ListComponent implements OnInit {
    gateways = null;

    constructor(private gatewayService: GatewayService) {}

    ngOnInit() {
        this.gatewayService.getAll()
            .subscribe(gateways => this.gateways = gateways);
    }

    deleteGateWay(id: string) {
        this.gatewayService.delete(id)           
            .subscribe(() => this.gateways = this.gateways.filter(x => x.gatewaySerialNumber !== id));
    }
}