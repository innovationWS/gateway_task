﻿
export class Device {
    uid: string;
    vendor: string;
    creationDate: string;
    status: boolean;
    gatewaySerialNumber: string;
}