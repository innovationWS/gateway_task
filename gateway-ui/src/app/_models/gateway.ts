﻿export class Gateway {
    gatewaySerialNumber: string;
    gatewayName: string;
    ipAddress: string;
    associatedDevice: string;
}