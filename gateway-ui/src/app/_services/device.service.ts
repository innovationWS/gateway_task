﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Device } from '@app/_models';

const baseUrl = `${environment.apiUrl}`;

@Injectable({ providedIn: 'root' })
export class DeviceService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Device[]>(baseUrl+'/device/get-all-Devices');
    }

    create(params) {
        return this.http.post(baseUrl+'/device/add-device', params);
    }

    delete(deviceUid: string,serialNumber: string) {
        return this.http.delete(baseUrl+'/device/delete-device/'+deviceUid+'/'+serialNumber);
    }
}