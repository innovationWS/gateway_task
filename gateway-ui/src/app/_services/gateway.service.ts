﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Gateway } from '@app/_models';

const baseUrl = `${environment.apiUrl}`;

@Injectable({ providedIn: 'root' })
export class GatewayService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Gateway[]>(baseUrl+'/gate/get-all-gateways');
    }

    getAllContainsLessThanTenDevices() {
        return this.http.get<Gateway[]>(baseUrl+'/gate/get-all-gateways-contain-less-than-ten');
    }

    getById(id: string) {
        return this.http.get<Gateway>(`${baseUrl}/${id}`);
    }

    create(params) {
        return this.http.post(baseUrl+'/gate/add-gateway', params);
    }

    update(id: string, params) {
        return this.http.put(`${baseUrl}/${id}`, params);
    }

    delete(gatewaySerialNumber: string) {
        return this.http.delete(baseUrl+'/gate/delete-gateway/'+gatewaySerialNumber);
    }
}