import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutDevicesComponent } from './layout-devices.component';
import { ListDevicesComponent } from './list-devices.component';
import { AddDeviceComponent } from './add-device.component';

const routes: Routes = [
    {
        path: '', component: LayoutDevicesComponent,
        children: [
            { path: '', component: ListDevicesComponent },
            { path: 'add', component: AddDeviceComponent },
      
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DevicesRoutingModule { }