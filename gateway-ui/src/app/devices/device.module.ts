import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { DevicesRoutingModule } from './device-routing.module';
import { LayoutDevicesComponent } from './layout-devices.component';
import { ListDevicesComponent } from './list-devices.component';
import { AddDeviceComponent } from './add-device.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        DevicesRoutingModule
    ],
    declarations: [
        LayoutDevicesComponent,
        ListDevicesComponent,
        AddDeviceComponent
    ]
})
export class DevicesModule { }