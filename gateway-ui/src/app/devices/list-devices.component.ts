﻿import { Component, OnInit } from '@angular/core';
import { DeviceService } from '@app/_services';

@Component({ templateUrl: 'list-devices.component.html' })
export class ListDevicesComponent implements OnInit {
    devices = null;

    constructor(private deviceService: DeviceService) {}

    ngOnInit() {
        this.deviceService.getAll()
            .subscribe(devices => this.devices = devices);
    }

    deleteDevice(id: string,serialNumber: string) {
        this.deviceService.delete(id,serialNumber)           
            .subscribe(() => this.devices = this.devices.filter(x => x.uid !== id));
    }
}