﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { GatewayService, AlertService,DeviceService } from '@app/_services';
import { Device } from '@app/_models';

@Component({ templateUrl: 'add-device.component.html' })
export class AddDeviceComponent implements OnInit {
    form: FormGroup;
    id: string;
  
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private gatewayService: GatewayService,
        private deviceService: DeviceService,
        private alertService: AlertService
    ) {}
    gateways = null;
    ngOnInit() {

        this.getAllGateWays();
        this.id = this.route.snapshot.params['id'];
        this.form = this.formBuilder.group({
            vendor: ['', Validators.required],
            status: ['', Validators.required],
            gatewaySerialNumber: ['', Validators.required]          
        });
    }

    getAllGateWays(){
        this.gatewayService.getAllContainsLessThanTenDevices()
            .subscribe(gateways => this.gateways = gateways);
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;
        this.addDevice();
        
    }

    private addDevice() {        
        this.deviceService.create(this.createDeviceObject())
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('Device added', { keepAfterRouteChange: true });
                    this.router.navigate(['../'], { relativeTo: this.route });
                },
                error: error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            });
    }

    createDeviceObject():Device{
        let device: Device =new Device();
        device.gatewaySerialNumber = this.form.value.gatewaySerialNumber;
        device.vendor = this.form.value.vendor;
        device.status = (this.form.value.status ==1?true:false);
        return device;
    }
}