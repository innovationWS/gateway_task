import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';

const gatewaysModule = () => import('./gateways/gateways.module').then(x => x.GatewaysModule);
const devicesModule = () => import('./devices/device.module').then(x => x.DevicesModule);


const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'gateways', loadChildren: gatewaysModule },
    { path: 'devices', loadChildren: devicesModule },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }