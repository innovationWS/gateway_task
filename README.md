could you please read this instruction to run gateway project

-- Backend running
1- import project as maven project in sts or intellij
2- run mvn clean  install
3- start project from entery point class "GatewayApplication.java"
4- check project running on port 8080


-- for running gateway UI project
1- open project under folder gateway\gateway-ui
2- npm install
3- npm start 
4- run in comand prompt "chrome.exe --user-data-dir="C:/Chrome dev session" --disable-web-security" to open chrome with disable-web-security
5- http://localhost:4200/