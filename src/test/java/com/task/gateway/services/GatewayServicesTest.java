package com.task.gateway.services;

import com.task.gateway.entity.DeviceData;
import com.task.gateway.entity.GateWayData;
import com.task.gateway.repository.DevicesRepository;
import com.task.gateway.repository.GatewayRepository;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class GatewayServicesTest {
    private final GatewayRepository gatewayRepositoryMock;

    private final GatewayServices gatewayServices;

    public GatewayServicesTest() {
        this.gatewayRepositoryMock = Mockito.mock(GatewayRepository.class);
        this.gatewayServices = new GatewayServices(gatewayRepositoryMock);
    }

    @Test
    public void deleteGateWayTest_sendGatewaySerialNumber_gatewayRepositoryDeleteByIdCalled() {
        doNothing().when(gatewayRepositoryMock).deleteById(anyString());
        gatewayServices.deleteGateWay("GatewaySerialNumber");
        verify(gatewayRepositoryMock, times(1)).deleteById(anyString());
    }

    @Test
    public void addGateWayTest_sendGateWayData_saveMethodCalled() {
        when(gatewayRepositoryMock.save(any(GateWayData.class))).thenReturn(new GateWayData());
        GateWayData retValue = gatewayServices.addGateWay(new GateWayData());
        Assert.assertNotNull(retValue);
        verify(gatewayRepositoryMock, times(1)).save(any(GateWayData.class));
    }

    @Test
    public void getAllGatewaysDataTest_findAllMethodCalled() {
        when(gatewayRepositoryMock.findAll()).thenReturn(new ArrayList<GateWayData>());
        List<GateWayData> retValue = gatewayServices.getAllGatewaysData();
        Assert.assertNotNull(retValue);
        verify(gatewayRepositoryMock, times(1)).findAll();
    }

    @Test
    public void updateAssociatedDeviceDecrementTest_sendGatewaySerialNumber_updateAssociatedDeviceByDecrementMethodCalled() {
        doNothing().when(gatewayRepositoryMock).updateAssociatedDeviceByDecrement(anyString());
        gatewayServices.updateAssociatedDeviceDecrement("serial number");
        verify(gatewayRepositoryMock, times(1)).updateAssociatedDeviceByDecrement(anyString());
    }

    @Test
    public void updateAssociatedDeviceTest_sendGatewaySerialNumber_updateAssociatedDeviceByIncrementMethodCalled() {
        doNothing().when(gatewayRepositoryMock).updateAssociatedDeviceByIncrement(anyString());
        gatewayServices.updateAssociatedDevice("serial number");
        verify(gatewayRepositoryMock, times(1)).updateAssociatedDeviceByIncrement(anyString());
    }

}