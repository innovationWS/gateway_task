package com.task.gateway.services;

import com.task.gateway.entity.DeviceData;
import com.task.gateway.repository.DevicesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class DevicesServicesTest  {
    private final DevicesRepository devicesRepositoryMock;
    private final DevicesServices devicesServices;
    public DevicesServicesTest() {
        this.devicesRepositoryMock = Mockito.mock(DevicesRepository.class);
        this.devicesServices = new DevicesServices(devicesRepositoryMock);
    }

    @Test
    public void deleteDeviceTest_sendUID_deviceRepositeryCalled() {
        doNothing().when(devicesRepositoryMock).deleteById(anyString());
        devicesServices.deleteDevice("uid");
        verify(devicesRepositoryMock,times(1)).deleteById(anyString());
    }

    @Test
    public void addDeviceTest_sendDeviceData_saveMethodCalled() {
        when(devicesRepositoryMock.save(any(DeviceData.class))).thenReturn(new DeviceData());
        DeviceData retValue =  devicesServices.addDevice(new DeviceData());
        Assert.assertNotNull(retValue);
        verify(devicesRepositoryMock,times(1)).save(any(DeviceData.class));
    }

    @Test
    public void getAllDevicesTest_findAllMethodCalled() {
        when(devicesRepositoryMock.findAll()).thenReturn(new ArrayList<DeviceData>());
        List<DeviceData> retValue =  devicesServices.getAllDevices();
        Assert.assertNotNull(retValue);
        verify(devicesRepositoryMock,times(1)).findAll();
    }

}