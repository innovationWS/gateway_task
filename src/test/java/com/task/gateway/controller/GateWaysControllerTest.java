package com.task.gateway.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.task.gateway.entity.DeviceData;
import com.task.gateway.entity.GateWayData;
import com.task.gateway.services.DevicesServices;
import com.task.gateway.services.GatewayServices;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GateWaysControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private GatewayServices gatewayServices;

    @Test
    public void getAllGateWaysTest_returnAllGateWaysData() throws Exception {
        GateWayData gateWayData = new GateWayData();
        gateWayData.setGatewaySerialNumber("serial Numer");
        List<GateWayData> gateWayList = new ArrayList<>();
        gateWayList.add(gateWayData);

        Mockito.when(gatewayServices.getAllGatewaysData()).thenReturn(gateWayList);
        mvc.perform(MockMvcRequestBuilders
                .get("/gate/get-all-gateways")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").exists());
    }

    @Test
    public void getAllGateWaysContainLessThanTenDevicesTest_returnAllGateWaysData() throws Exception {
        GateWayData gateWayData = new GateWayData();
        gateWayData.setGatewaySerialNumber("serial Numer");
        List<GateWayData> gateWayList = new ArrayList<>();
        gateWayList.add(gateWayData);

        Mockito.when(gatewayServices.getAllGatewaysDataContainLessThanTenDevices()).thenReturn(gateWayList);
        mvc.perform(MockMvcRequestBuilders
                .get("/gate/get-all-gateways-contain-less-than-ten")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").exists());
    }

    @Test
    public void addGateWayTest_sendGateWayDataObject_addGateWayAndSendNoContentStatusCode() throws Exception {
        Mockito.when(gatewayServices.addGateWay(any(GateWayData.class))).thenReturn(new GateWayData());
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .post("/gate/add-gateway", new DeviceData())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(asJsonString(new DeviceData()))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        assertEquals(204, statusCode);
    }

    @Test
    public void deleteGateWayTest_sendGatewaySerialNumber_DeleteGateWayAndSendNoContentStatusCode() throws Exception {
        doNothing().when(gatewayServices).deleteGateWay(anyString());

        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .delete("/gate/delete-gateway/{gatewaySerialNumber}", "gateway serial number")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        assertEquals(204, statusCode);
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}