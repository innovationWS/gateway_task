package com.task.gateway.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.task.gateway.entity.DeviceData;
import com.task.gateway.services.DevicesServices;
import com.task.gateway.services.GatewayServices;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DevicesControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private DevicesServices devicesServices;
    @MockBean
    private GatewayServices gatewayServices;

    @Test
    public void getAllDevices_returnAllDevicesData() throws Exception {
        DeviceData deviceData = new DeviceData();
        deviceData.setUid("UID");
        List<DeviceData> deviceList = new ArrayList<>();
        deviceList.add(deviceData);

        Mockito.when(devicesServices.getAllDevices()).thenReturn(deviceList);
        mvc.perform(MockMvcRequestBuilders
                .get("/device/get-all-Devices")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").exists());
    }

    @Test
    public void addDeviceTest_sendDeviceDataObject_addDeviceAndSendNoContentStatusCode() throws Exception {
        Mockito.when(devicesServices.addDevice(any(DeviceData.class))).thenReturn(new DeviceData());
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .post("/device/add-device",new DeviceData())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(asJsonString(new DeviceData()))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        assertEquals(204, statusCode);
    }

    @Test
    public void deleteDeviceTest_sendUIDAndGatewaySerialNumber_DeleteDeviceAndSendNoContentStatusCode() throws Exception {
        doNothing().when(devicesServices).deleteDevice(anyString());
        doNothing().when(gatewayServices).updateAssociatedDeviceDecrement(anyString());

        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .delete("/device/delete-device/{deviceuid}/{gatewaySerialNumber}", "uid", "gateway serial number")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        assertEquals(204, statusCode);
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}