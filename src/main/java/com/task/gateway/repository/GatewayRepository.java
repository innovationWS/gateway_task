package com.task.gateway.repository;


import com.task.gateway.entity.GateWayData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Transactional
public interface GatewayRepository extends JpaRepository<GateWayData, String>
{
    @Query(" select gateway from GateWayData gateway where gateway.associatedDevice <=10 ")
    List<GateWayData> findGateWaysContainLessThanTenDevices();

    @Modifying
    @Query(" update GateWayData set associatedDevice = associatedDevice+1 where gatewaySerialNumber= :gatewaySerialNumber")
    void updateAssociatedDeviceByIncrement(@Param("gatewaySerialNumber") String gatewaySerialNumber);

    @Modifying
    @Query(" update GateWayData set associatedDevice = associatedDevice-1 where gatewaySerialNumber= :gatewaySerialNumber")
    void updateAssociatedDeviceByDecrement(@Param("gatewaySerialNumber") String gatewaySerialNumber);
}
