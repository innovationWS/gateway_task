package com.task.gateway.repository;


import com.task.gateway.entity.DeviceData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;


@Transactional
public interface DevicesRepository extends JpaRepository<DeviceData, String>
{
}
