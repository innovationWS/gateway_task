package com.task.gateway.entity;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name="device_data")
@Data
@Getter
@Setter
public class DeviceData implements Serializable {

	private static final long serialVersionUID = -5849782591339196929L;
    @Id
    @Column(name="UID")
	private String uid;
    @Column(name="vendor")
    private String vendor;
    @Column(name="creation_date")
    private Date creationDate;
    @Column(name="status")
    private boolean status;
    @Column(name="gateway_serial_number")
    private String gatewaySerialNumber;
}


