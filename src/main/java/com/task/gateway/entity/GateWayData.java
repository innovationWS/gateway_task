package com.task.gateway.entity;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;




@Entity
@Table(name="gateway_data")
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GateWayData  implements Serializable {

	private static final long serialVersionUID = -5849782591339196929L;
    @Id
    @Column(name="gateway_serial_number")
	private String gatewaySerialNumber;
    @Column(name="gateway_name")
    private String gatewayName;
    @Column(name="ip_address")
    private String ipAddress;
    @Column(name="associated_device")
    private int associatedDevice;
}


