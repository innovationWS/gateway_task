package com.task.gateway.controller;

import com.task.gateway.entity.DeviceData;
import com.task.gateway.services.DevicesServices;
import com.task.gateway.services.GatewayServices;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.UUID;
@CrossOrigin
@RestController
@RequestMapping("/device")
public class DevicesController {

    private final DevicesServices devicesServices;
    private final GatewayServices gatewayServices;

    public DevicesController(DevicesServices devicesServices,GatewayServices gatewayServices) {
        this.devicesServices = devicesServices;
        this.gatewayServices=gatewayServices;
    }

    @GetMapping("/get-all-Devices")
    public ResponseEntity<List<DeviceData>> getAllDevices() {
        return ResponseEntity.ok(devicesServices.getAllDevices());
    }

    @PostMapping("/add-device")
    public ResponseEntity addDevice(@RequestBody DeviceData deviceData) {
        UUID uuid = UUID.randomUUID();
        deviceData.setUid(uuid.toString());
        deviceData.setCreationDate(new Date(System.currentTimeMillis()));
        devicesServices.addDevice(deviceData);
        this.gatewayServices.updateAssociatedDevice(deviceData.getGatewaySerialNumber());
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/delete-device/{deviceuid}/{gatewaySerialNumber}")
    public ResponseEntity deleteDevice(@PathVariable String deviceuid,@PathVariable String gatewaySerialNumber) {
        devicesServices.deleteDevice(deviceuid);
        this.gatewayServices.updateAssociatedDeviceDecrement(gatewaySerialNumber);
        return ResponseEntity.noContent().build();
    }
}
