package com.task.gateway.controller;

import com.task.gateway.entity.GateWayData;
import com.task.gateway.services.GatewayServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
@CrossOrigin
@RestController
@RequestMapping("/gate")
public class GateWaysController {

    private final Logger logger = LoggerFactory.getLogger(GateWaysController.class);

    private final GatewayServices gatewayServices;

    public GateWaysController(GatewayServices gatewayServices) {
        this.gatewayServices = gatewayServices;
    }

    @GetMapping("/get-all-gateways")
    public ResponseEntity<List<GateWayData>> getAllGateWays() {
        return ResponseEntity.ok(gatewayServices.getAllGatewaysData());
    }

    @GetMapping("/get-all-gateways-contain-less-than-ten")
    public ResponseEntity<List<GateWayData>> getAllGateWaysContainLessThanTenDevices() {
        return ResponseEntity.ok(gatewayServices.getAllGatewaysDataContainLessThanTenDevices());
    }

    @PostMapping("/add-gateway")
    public ResponseEntity addGateWay(@RequestBody GateWayData gateWayData) {
        UUID uuid = UUID.randomUUID();
        gateWayData.setGatewaySerialNumber(uuid.toString());
        gatewayServices.addGateWay(gateWayData);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/delete-gateway/{gatewaySerialNumber}")
    public ResponseEntity deleteGateWay(@PathVariable String gatewaySerialNumber) {
        gatewayServices.deleteGateWay(gatewaySerialNumber);
        return ResponseEntity.noContent().build();
    }
}
