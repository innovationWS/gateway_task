package com.task.gateway.services;

import com.task.gateway.entity.GateWayData;
import com.task.gateway.repository.GatewayRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class GatewayServices {
    private final GatewayRepository gatewayRepository;

    public GatewayServices(GatewayRepository gatewayRepository) {
        this.gatewayRepository = gatewayRepository;
    }

    public List<GateWayData> getAllGatewaysData() {
        return gatewayRepository.findAll();
    }

    public List<GateWayData> getAllGatewaysDataContainLessThanTenDevices() {
        return gatewayRepository.findGateWaysContainLessThanTenDevices();
    }

    public void updateAssociatedDevice(String gatewaySerialNumber) {
         gatewayRepository.updateAssociatedDeviceByIncrement(gatewaySerialNumber);
    }

    public void updateAssociatedDeviceDecrement(String gatewaySerialNumber) {
        gatewayRepository.updateAssociatedDeviceByDecrement(gatewaySerialNumber);
    }

    public GateWayData addGateWay(GateWayData gateWayData) {
        return gatewayRepository.save(gateWayData);
    }

    public void deleteGateWay(String gatewaySerialNumber) {
        gatewayRepository.deleteById(gatewaySerialNumber);
    }
}
