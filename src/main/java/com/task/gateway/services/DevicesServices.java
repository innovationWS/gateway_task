package com.task.gateway.services;

import com.task.gateway.entity.DeviceData;
import com.task.gateway.repository.DevicesRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DevicesServices {
    private final DevicesRepository devicesRepository;

    public DevicesServices(DevicesRepository devicesRepository) {
        this.devicesRepository = devicesRepository;
    }

    public List<DeviceData> getAllDevices() {
        return devicesRepository.findAll();
    }

    public DeviceData addDevice(DeviceData deviceData) {
        return devicesRepository.save(deviceData);
    }

    public void deleteDevice(String UID) {
        devicesRepository.deleteById(UID);
    }
}
